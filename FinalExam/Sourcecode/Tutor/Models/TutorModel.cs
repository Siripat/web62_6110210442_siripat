using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Tutor.Models
{
	public class NewsUser : IdentityUser{
 		public string FirstName { get; set;}
 		public string LastName { get; set;}
 	}

	public class NewSubject {
		public int NewSubjectID { get; set;}
		public string SujectName { get; set;}
	}

	public class Std {
		public int StdID { get; set;}

		public int NewSubjectID { get; set;}
		public NewSubject NewSub { get; set;}

		[DataType(DataType.Date)]
		public string PostDate { get; set;}
		public string StdName { get; set;}
		public string StdNickname { get; set;}
		public string Place { get; set;}
		public string Hour { get; set;}
		public string WageRate { get; set;}
		public string PhoneNumber { get; set;}
		public string Facebook { get; set;}

		public string NewsUserId {get; set;}
 		public NewsUser postUser {get; set;}
	}
}