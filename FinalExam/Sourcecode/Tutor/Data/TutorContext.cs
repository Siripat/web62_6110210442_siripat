using Tutor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Tutor.Data
{
	public class TutorContext : IdentityDbContext<NewsUser>
	{
		public DbSet<Std> NewList { get; set;}
		public DbSet<NewSubject> NewSubject { get; set;}
		public DbSet<NewsUser> NewsUser { get; set;}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
 		{
 			optionsBuilder.UseSqlite(@"Data source=Tutor.db");
 		}

	}
}