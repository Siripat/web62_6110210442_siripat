using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Tutor.Data;
using Tutor.Models;

namespace Tutor.Pages.NewStdAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly Tutor.Data.TutorContext _context;

        public DetailsModel(Tutor.Data.TutorContext context)
        {
            _context = context;
        }

        public Std Std { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Std = await _context.NewList
                .Include(s => s.NewSub)
                .Include(s => s.postUser).FirstOrDefaultAsync(m => m.StdID == id);

            if (Std == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
