using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Tutor.Data;
using Tutor.Models;

namespace Tutor.Pages.NewStdAdmin
{
    public class EditModel : PageModel
    {
        private readonly Tutor.Data.TutorContext _context;

        public EditModel(Tutor.Data.TutorContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Std Std { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Std = await _context.NewList
                .Include(s => s.NewSub)
                .Include(s => s.postUser).FirstOrDefaultAsync(m => m.StdID == id);

            if (Std == null)
            {
                return NotFound();
            }
           ViewData["NewSubjectID"] = new SelectList(_context.NewSubject, "NewSubjectID", "SujectName");
           ViewData["NewsUserId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Std).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StdExists(Std.StdID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool StdExists(int id)
        {
            return _context.NewList.Any(e => e.StdID == id);
        }
    }
}
