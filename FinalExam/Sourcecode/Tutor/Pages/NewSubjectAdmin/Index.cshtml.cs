using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Tutor.Data;
using Tutor.Models;

namespace Tutor.Pages.NewSubjectAdmin
{
    public class IndexModel : PageModel
    {
        private readonly Tutor.Data.TutorContext _context;

        public IndexModel(Tutor.Data.TutorContext context)
        {
            _context = context;
        }

        public IList<NewSubject> NewSubject { get;set; }

        public async Task OnGetAsync()
        {
            NewSubject = await _context.NewSubject.ToListAsync();
        }
    }
}
