using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Tutor.Data;
using Tutor.Models;

namespace Tutor.Pages.NewSubjectAdmin
{
    public class CreateModel : PageModel
    {
        private readonly Tutor.Data.TutorContext _context;

        public CreateModel(Tutor.Data.TutorContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public NewSubject NewSubject { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.NewSubject.Add(NewSubject);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}