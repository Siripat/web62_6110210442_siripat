using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Tutor.Data;
using Tutor.Models;

namespace Tutor.Pages.NewSubjectAdmin
{
    public class EditModel : PageModel
    {
        private readonly Tutor.Data.TutorContext _context;

        public EditModel(Tutor.Data.TutorContext context)
        {
            _context = context;
        }

        [BindProperty]
        public NewSubject NewSubject { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            NewSubject = await _context.NewSubject.FirstOrDefaultAsync(m => m.NewSubjectID == id);

            if (NewSubject == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(NewSubject).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NewSubjectExists(NewSubject.NewSubjectID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool NewSubjectExists(int id)
        {
            return _context.NewSubject.Any(e => e.NewSubjectID == id);
        }
    }
}
