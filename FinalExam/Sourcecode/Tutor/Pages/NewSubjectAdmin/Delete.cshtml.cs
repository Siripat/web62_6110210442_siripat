using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Tutor.Data;
using Tutor.Models;

namespace Tutor.Pages.NewSubjectAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly Tutor.Data.TutorContext _context;

        public DeleteModel(Tutor.Data.TutorContext context)
        {
            _context = context;
        }

        [BindProperty]
        public NewSubject NewSubject { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            NewSubject = await _context.NewSubject.FirstOrDefaultAsync(m => m.NewSubjectID == id);

            if (NewSubject == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            NewSubject = await _context.NewSubject.FindAsync(id);

            if (NewSubject != null)
            {
                _context.NewSubject.Remove(NewSubject);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
