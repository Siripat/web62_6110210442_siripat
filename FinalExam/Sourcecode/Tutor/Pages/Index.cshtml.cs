﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Tutor.Models;
using Tutor.Data;

namespace Tutor.Pages
{
    public class IndexModel : PageModel
    {
    	private readonly TutorContext db;
    	public IndexModel(TutorContext db) => this.db = db;
    	public List<NewSubject> newSubject { get; set; } = new List<NewSubject>();
    	public List<Std> newList { get; set; } = new List<Std>();
        public List<NewsUser> newsUser { get; set; } = new List<NewsUser>();
        public void OnGet()
        {
        	newSubject = db.NewSubject.ToList();
        	newList = db.NewList.ToList();
            newsUser = db.NewsUser.ToList();
        }
    }
}
